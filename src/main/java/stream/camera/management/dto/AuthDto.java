package stream.camera.management.dto;

import java.time.LocalDate;

public class AuthDto {

    private String uuid;
    private String[] ipAddressList;
    private String username;
    private String password;
    private LocalDate createAt;
    private String createBy;
    private LocalDate updateAt;
    private String updateBy;

    public AuthDto() {
        super();
    }

    public AuthDto(String uuid, String[] ipAddressList, String username, String password, LocalDate createAt, String createBy,
                   LocalDate updateAt, String updateBy) {
        this.uuid = uuid;
        this.ipAddressList = ipAddressList;
        this.username = username;
        this.password = password;
        this.createAt = createAt;
        this.createBy = createBy;
        this.updateAt = updateAt;
        this.updateBy = updateBy;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String[] getIpAddressList() {
        return ipAddressList;
    }

    public void setIpAddressList(String[] ipAddressList) {
        this.ipAddressList = ipAddressList;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getCreateAt() {
        return this.createAt;
    }

    public void setCreateAt(LocalDate createAt) {
        this.createAt = createAt;
    }

    public String getCreateBy() {
        return this.createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public LocalDate getUpdateAt() {
        return this.updateAt;
    }

    public void setUpdateAt(LocalDate updateAt) {
        this.updateAt = updateAt;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public String toString() {
        return "{" + " \"uuid\":\"" + uuid + "\"" + ", \"ipAddressList\":[" + buildIpAddressListJson() + "]"
                + ", \"username\":\"" + username + "\"" + ", \"password\":\"" + password + "\""
                + " \"createAt\":\"" + createAt + "\"" + " \"createBy\":\"" + createBy + "\"" +
                " \"updateAt\":\"" + updateAt + "\"" + ", \"updateBy\":\"" + updateBy + "\"" + "}";
    }

    private String buildIpAddressListJson() {
        String jsonString = "";
        if (ipAddressList != null) {
            for (int i = 0; i < ipAddressList.length; i++) {
                jsonString = jsonString + "\"" + ipAddressList[i] + "\"";
                if (i != ipAddressList.length - 1)
                    jsonString += ",";
            }
        } else
            jsonString = "null";
        return jsonString;
    }
}

package stream.camera.management.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import org.java_websocket.client.WebSocketClient;
import stream.camera.management.dto.*;
import stream.camera.management.service.AuthService;
import stream.camera.management.service.DeviceInfoService;

@RestController
@RequestMapping("/camera/device-info")
@CrossOrigin(origins = "http://localhost:3000")
public class DeviceInfoController {

    private static String ONVIF_SERVICE = "onvif-service";

    private final Logger log = LoggerFactory.getLogger(DeviceInfoController.class);

    @Autowired
    private AuthService authService;

    @Autowired
    private DeviceInfoService deviceInfoService;

    @GetMapping("all")
    public String getAllDeviceInfo() {
        try {
            List<DeviceInfoPlainDto> deviceInfoPlainDtoList = deviceInfoService.getAllDeviceInfo();
            log.debug("Get all device info successfully");
            return deviceInfoPlainDtoList.toString();
        } catch (Exception e) {
            log.error("Failed to get all device info table rows due to " + e.getMessage());
            return "Failed to get all device info table rows due to " + e.getMessage();
        }

    }

    @GetMapping("{uuid}")
    public String getDeviceInfoByUuid(@PathVariable(value = "uuid") String uuid) {
        try {
            DeviceInfoPlainDto deviceInfoPlainDto = deviceInfoService.getDeviceInfoByUuid(uuid);
            log.debug("Get device info by uuid successfully");
            return deviceInfoPlainDto == null ? "No stored camera has the uuid in query" : deviceInfoPlainDto.toString();
        } catch (Exception e) {
            log.error("Failed to get device info row by uuid due to " + e.getMessage());
            return "Failed to get device info row by uuid due to " + e.getMessage();
        }
    }

    @GetMapping("/stream-url/{uuid}")
    public String getDeviceStreamUrlByUuid(@PathVariable(value = "uuid") String uuid) {
        try {
            String[] streamUrlList = deviceInfoService.getDeviceStreamUrlByUuid(uuid);
            if (streamUrlList == null || streamUrlList.length == 0)
                return "No stored camera has the uuid in query";
            log.debug("Get device stream url list by uuid  by uuid successfully");

            String outPut = "Find stream url list for uuid " + uuid + " is ";
            for (String streamUrl : streamUrlList) {
                outPut += "\n" + streamUrl;
            }
            return outPut;
        } catch (Exception e) {
            log.error("Failed to get device stream url list by uuid due to " + e.getMessage());
            return "Failed to get device stream url list by uuid due to " + e.getMessage();
        }
    }

    @GetMapping("/ip-address/{uuid}")
    public String getDeviceIpAddressByUuid(@PathVariable(value = "uuid") String uuid) {
        try {
            String[] ipAddressList = deviceInfoService.getDeviceIpAddressByUuid(uuid);
            if (ipAddressList == null || ipAddressList.length == 0)
                return "No stored camera has the uuid in query";

            String response = "Find device ip address for uuid " + uuid + " is ";
            String jsonString = "";
            if (ipAddressList != null) {
                for (int i = 0; i < ipAddressList.length; i++) {
                    jsonString = jsonString + "\"" + ipAddressList[i] + "\"";
                    if (i != ipAddressList.length - 1)
                        jsonString += ",";
                }
            } else
                jsonString = "null";

            response += jsonString;
            log.debug("Get device stream ip address by uuid successfully");
            return response;
        } catch (Exception e) {
            log.error("Failed to get device ip address by uuid due to " + e.getMessage());
            return "Failed to get device ip address by uuid due to " + e.getMessage();
        }
    }

    @PostMapping("")
    public String createDeviceInfo(@RequestBody List<DeviceInfoDto> deviceInfoDtoList) {
        try {
            deviceInfoService.createDeviceInfo(deviceInfoDtoList);
            log.debug("Create device_info table row(s) successfully");
            return "Successfully create the device_info table row(s)";
        } catch (Exception e) {
            log.error("Failed to create the device_info table row(s) due to " + e.getMessage());
            return "Failed to create the device_info table row(s) due to " + e.getMessage();
        }
    }

    @PutMapping("")
    public String updateDeviceInfo(@RequestBody List<DeviceInfoDto> deviceInfoDtoList) {
        try {
            deviceInfoService.updateDeviceInfo(deviceInfoDtoList);
            log.debug("Update device_info table row(s) successfully");
            return "Successfully update the device_info table row(s)";
        } catch (Exception e) {
            log.error("Failed to update the device_info table row(s) due to " + e.getMessage());
            return "Failed to update the device_info table row(s) due to " + e.getMessage();
        }
    }

    @DeleteMapping("")
    public String deleteDeviceInfo(@RequestBody List<String> uuidList) {
        try {
            deviceInfoService.deleteDeviceInfo(uuidList);
            log.debug("Delete device_info table row(s) successfully");
            return "Successfully delete the device_info table row(s)";
        } catch (Exception e) {
            log.error("Failed to delete the device_info table row(s) due to " + e.getMessage());
            return "Failed to delete the device_info table row(s) due to " + e.getMessage();
        }
    }

    @DeleteMapping("all")
    public String deleteAllDeviceInfo() {
        try {
            deviceInfoService.deleteAllDeviceInfo();
            log.debug("Delete all device_info table row(s) successfully");
            return "All device info are deleted successfully";
        } catch (Exception e) {
            log.error("Failed to delete all device_info table row(s) due to " + e.getMessage());
            return "Failed to delete all device_info table row(s) due to " + e.getMessage();
        }
    }

    @PostMapping("/connection")
    public String connectAndSaveDeviceInfo(@RequestBody AuthConnectionDto authConnectionDto) {
        DeviceInfoDto deviceInfoDto = new DeviceInfoDto();
        String nodejsUrI = "";
        try {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpPost postRequest = new HttpPost(
                    "http://localhost:9595/v1/camera/info");
            StringEntity input = new StringEntity(authConnectionDto.toString());
            input.setContentType("application/json");
            postRequest.setEntity(input);
            HttpResponse response = httpClient.execute(postRequest);
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }
            log.debug("Successfully send the request to onvif service");

            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));
            String cameraDeviceInfo = "";
            if ((cameraDeviceInfo = br.readLine()) != null) {
                log.info("Connected camera device info is {}", cameraDeviceInfo);
                JSONObject jsonObject = JSON.parseObject(cameraDeviceInfo);
                AuthDto authDto = new AuthDto();
                authDto.setUuid(authConnectionDto.getUuid());
                authDto.setUsername(authConnectionDto.getUsername());
                authDto.setPassword(authConnectionDto.getPassword());
                List<AuthDto> newAuthList = new ArrayList<>();
                newAuthList.add(authDto);
                authService.createAuth(newAuthList);



                deviceInfoDto.setUuid(authConnectionDto.getUuid());
                deviceInfoDto.setSerialNumber(jsonObject.getString("serialNumber"));
                deviceInfoDto.setModel(jsonObject.getString("model"));
                deviceInfoDto.setFirmwareVersion(jsonObject.getString("firmwareVersion"));

                LocalDate date = jsonObject.getString("buildDate") == null
                        ? LocalDate.now()
                        : LocalDate.parse(jsonObject.getString("buildDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                deviceInfoDto.setBuildDate(date);

                deviceInfoDto.setHardwareId(jsonObject.getString("hardwareId"));
                deviceInfoDto.setActive(true);

                JSONArray jsonArray = jsonObject.getJSONArray("rtspUrls");
                String[] streamListJson = new String[jsonArray.size()];
                for (int i = 0; i < jsonArray.size(); i++) {
                    streamListJson[i] = jsonArray.getString(i);
                }
                deviceInfoDto.setStreamUrlList(streamListJson);

                String[] ipAddressList = deviceInfoService.getDeviceIpAddressByUuid(authConnectionDto.getUuid());
                deviceInfoDto.setIpAddressList(ipAddressList == null ? new String[]{authConnectionDto.getIpAddress()} : ipAddressList);

                if (deviceInfoDto.getModel().equalsIgnoreCase("IP Webcam"))
                    nodejsUrI = deviceInfoDto.getStreamUrlList()[0];
                else {

                    String[] splitWord = streamListJson[0].split("/");
                    String host = splitWord[2];
                    splitWord[2] = authConnectionDto.getUsername() + ":" + authConnectionDto.getPassword() + "@"
                            + splitWord[2];
                    nodejsUrI = streamListJson[0].replaceFirst(host, splitWord[2]);
                    String[] streamUrlArr = { nodejsUrI };

                    deviceInfoDto.setStreamUrlList(streamUrlArr);
                }

                deviceInfoDto.setCreateBy(ONVIF_SERVICE);
            } else {
                throw new RuntimeException("Failed to get response from onvif service ");
            }

            String finalNodejsUrI = nodejsUrI;
            Thread mainThread = Thread.currentThread();
            Thread subThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    connectToNodejsBackend(deviceInfoDto, finalNodejsUrI, mainThread);
                }
            });
            subThread.start();

            try {
                synchronized (mainThread) {
                    mainThread.wait();
                }
            } catch (Exception e) {
                log.error("Thread lock error {}", e);
            }

            List<DeviceInfoDto> deviceInfoDtoList = new ArrayList<>();
            deviceInfoDtoList.add(deviceInfoDto);
            deviceInfoService.createDeviceInfo(deviceInfoDtoList);

            log.debug("Successfully connect to the camera and save the device_info");
            return "Successfully connect to the camera and save the device_info";
        } catch (Exception e) {
            log.error("Failed to connect to the camera and save the device_info due to " + e.getMessage());
            return "Failed to connect to the camera and save the device_info due to " + e.getMessage();
        }
    }

    private synchronized void connectToNodejsBackend(DeviceInfoDto deviceInfoDto, String nodejsUrI, Thread thread) {
        try {
            Thread.sleep(2000);
            WebSocketClient socketClient = new WebSocketClient(new URI("ws://localhost:8188/" + nodejsUrI)) {
                @Override
                public void onOpen(ServerHandshake serverHandshake) {
                    log.debug("open connection");
                }

                @Override
                public void onMessage(String codec) {
                    log.debug("Getting codec from nodejs backend which is {}", codec);
                    deviceInfoDto.setCodec(codec);
                    synchronized (thread) {
                        thread.notifyAll();
                    }
                }

                @Override
                public void onClose(int i, String s, boolean b) {
                    log.debug("close connection");
                }

                @Override
                public void onError(Exception e) {
                    synchronized (thread) {
                        thread.notifyAll();
                    }
                    throw new RuntimeException(e.getMessage());
                }
            };
            socketClient.connect();
        } catch (Exception e) {
            log.error("Fail to get codec due to {}", e);
        }
    }

}
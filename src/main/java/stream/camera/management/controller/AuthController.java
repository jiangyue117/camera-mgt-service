package stream.camera.management.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import stream.camera.management.dto.AuthDto;
import stream.camera.management.dto.AuthPlainDto;
import stream.camera.management.dto.DeviceInfoDto;
import stream.camera.management.service.AuthService;
import stream.camera.management.service.DeviceInfoService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/camera/auth")
@CrossOrigin(origins = "http://localhost:3000")
public class AuthController {
    private static String ONVIF_SERVICE = "onvif-service";

    private final Logger log = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private AuthService authService;

    @Autowired
    private DeviceInfoService deviceInfoService;

    @GetMapping("all")
    public String getAllAuth() {
        try {
            List<AuthPlainDto> authPlainDtoList = authService.getAllAuth();
            log.debug("Get all auth successfully");
            return authPlainDtoList.toString();
        } catch (Exception e) {
            log.error("Failed to get all auth table rows due to " + e.getMessage());
            return "Failed to get all auth table rows due to " + e.getMessage();
        }
    }

    @GetMapping("{uuid}")
    public String getAuthByUuid(@PathVariable(value = "uuid") String uuid) {
        try {
            AuthPlainDto authPlainDto = authService.getAuthByUuid(uuid);
            log.debug("Get row by uuid successfully");
            return authPlainDto == null ? "No stored camera has the uuid in query" : authPlainDto.toString();
        } catch (Exception e) {
            log.error("Failed to get auth row by uuid due to " + e.getMessage());
            return "Failed to get auth row by uuid due to " + e.getMessage();
        }
    }

    @PostMapping("")
    public String createAuth(@RequestBody List<AuthDto> authDtoList) {
        try {
            authService.createAuth(authDtoList);
            log.debug("Create the auth table row(s) successfully");
            return "Successfully create the auth table row(s)";
        } catch (Exception e) {
            log.error("Failed to create auth table row(s) due to " + e.getMessage());
            return "Failed to create auth table row(s) due to " + e.getMessage();
        }
    }

    @PutMapping("")
    public String updateAuth(@RequestBody List<AuthDto> authDtoList) {
        try {
            authService.updateAuth(authDtoList);
            log.debug("Update the auth table row(s) successfully");
            return "Successfully update the auth table row(s)";
        } catch (Exception e) {
            log.error("Failed to update auth table row(s) due to " + e.getMessage());
            return "Failed to update auth table row(s) due to " + e.getMessage();
        }
    }

    @DeleteMapping("")
    public String deleteAuth(@RequestBody List<String> uuidList) {
        try {
            authService.deleteAuth(uuidList);
            log.debug("Delete the auth table row(s) successfully");
            return "Successfully delete the auth table row(s)";
        } catch (Exception e) {
            log.error("Failed to delete auth table row(s) due to " + e.getMessage());
            return "Failed to delete auth table row(s) due to " + e.getMessage();
        }
    }

    @DeleteMapping("all")
    public String deleteAllAuth() {
        try {
            authService.deleteAllAuth();
            log.debug("Delete all auth table row(s) successfully");
            return "All Auth are deleted successfully";
        } catch (Exception e) {
            log.error("Failed to delete All auth table rows due to " + e.getMessage());
            return "Failed to delete All auth table rows due to " + e.getMessage();
        }
    }

    @PostMapping("/discovery")
    public List<AuthDto> discoverCamera(@RequestParam(name = "timeout", required = false) Integer timeout) throws Exception {
        try {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpGet request = new HttpGet("http://localhost:9095/v1/camera/discovery");
            if (timeout != null)
                request.setHeader("timeout", timeout.toString());
            HttpResponse response = httpClient.execute(request);
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }
            log.debug("Successfully send the request to onvif service");

            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));
            String cameraAuthInfo = "";
            List<AuthDto> authDtoList = new ArrayList<>();
            List<DeviceInfoDto> deviceInfoDtoList = new ArrayList<>();

            if ((cameraAuthInfo = br.readLine()) != null) {
                JSONArray jsonArray = JSON.parseArray(cameraAuthInfo);
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    AuthDto authDto = new AuthDto();
                    DeviceInfoDto deviceInfoDto = new DeviceInfoDto();

                    authDto.setUuid(jsonObject.getString("uuid"));
                    deviceInfoDto.setUuid(jsonObject.getString("uuid"));

                    JSONArray array = jsonObject.getJSONArray("ips");
                    if (array != null && array.size() != 0) {
                        String[] ipAddressArray = new String[array.size()];
                        for (int index = 0; index < array.size(); index++)
                            ipAddressArray[index] = array.getString(0);
                        authDto.setIpAddressList(ipAddressArray);
                        deviceInfoDto.setIpAddressList(ipAddressArray);
                    } else
                        continue;

                    LocalDate createTime = LocalDate.now();
                    authDto.setCreateBy(ONVIF_SERVICE);
                    authDto.setCreateAt(createTime);

                    deviceInfoDto.setModel(jsonObject.getString("name"));
                    deviceInfoDto.setHardwareId(jsonObject.getString("hardware"));
                    deviceInfoDto.setActive(true);
                    deviceInfoDto.setCreateBy(ONVIF_SERVICE);
                    deviceInfoDto.setCreateAt(createTime);
                    authDtoList.add(authDto);
                    deviceInfoDtoList.add(deviceInfoDto);
                }
            } else {
                throw new RuntimeException("Failed to get response from onvif service ");
            }
            authService.createAuth(authDtoList);
            deviceInfoService.createDeviceInfo(deviceInfoDtoList);

            log.debug("Successfully detect the cameras from the network and save the auth to database");
            return authDtoList;
        } catch (Exception e) {
            log.error("Failed to detect the cameras from the network due to " + e.getMessage());
            throw e;
        }
    }
}

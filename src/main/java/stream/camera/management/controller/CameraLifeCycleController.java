package stream.camera.management.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import stream.camera.management.dto.DeviceInfoPlainDto;
import stream.camera.management.service.DeviceInfoService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class CameraLifeCycleController {

    @Autowired
    private DeviceInfoService deviceInfoService;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @PostMapping("/update-cameras")
    @ResponseBody
    public String updateCameras(@RequestBody String message) {
        JSONArray discoveredDevices = JSON.parseArray(message);
        List<String> discoveredUuidList = new ArrayList<>();
        for (int i = 0; i < discoveredDevices.size(); i++) {
            JSONObject device = discoveredDevices.getJSONObject(i);
            if (device.getBoolean("valid")) {
                discoveredUuidList.add(device.getString("uuid"));
            }
        }
        List<String> existingUuidList = deviceInfoService.getAllDeviceIds();
        List<String> approvedCameraIdList = determineApprovedCameras(discoveredUuidList, existingUuidList);
        JSONArray array = new JSONArray();
        if (approvedCameraIdList.size() > 0) {
            for (String uuid : approvedCameraIdList) {
                for (int j = 0; j < discoveredDevices.size(); j++) {
                    JSONObject device = discoveredDevices.getJSONObject(j);
                    if (uuid.equals(device.getString("uuid"))) {
                        DeviceInfoPlainDto deviceInDB = deviceInfoService.getDeviceInfoByUuid(uuid);
                        device.remove("types");
                        device.put("ip", device.getJSONArray("ips").getString(0));
                        device.remove("ips");
                        device.remove("valid");
                        JSONArray urlArray = new JSONArray();
                        String[] streamUrls = deviceInDB.getStreamUrlList();
                        urlArray.addAll(Arrays.asList(streamUrls));
                        device.put("serial_number", deviceInDB.getSerialNumber());
                        device.put("firmware_version", deviceInDB.getFirmwareVersion());
                        device.put("build_date", deviceInDB.getBuildDate());
                        device.put("hardware_id", deviceInDB.getHardwareId());
                        device.put("stream_urls", urlArray);
                        device.put("codec", deviceInDB.getCodec());
                        device.put("connected", true);
                        boolean active = deviceInDB.getActive();
                        if (!active) {
                            deviceInfoService.updateDeviceActive(uuid, true);
                        }
                        device.put("camera_status", true);
                        array.add(device);
                    }
                }
            }
        }
        messagingTemplate.convertAndSend("/topic/approved-cameras", array.toString());
        List<String> notApprovedCameraIdList = determineNotApprovedCameras(discoveredUuidList, existingUuidList);
        array = new JSONArray();
        if (notApprovedCameraIdList.size() > 0) {
            for (String uuid : notApprovedCameraIdList) {
                for (int j = 0; j < discoveredDevices.size(); j++) {
                    JSONObject device = discoveredDevices.getJSONObject(j);
                    if (uuid.equals(device.getString("uuid"))) {
                        device.remove("types");
                        device.put("ip", device.getJSONArray("ips").getString(0));
                        device.remove("ips");
                        device.remove("valid");
                        device.put("serial_number", "");
                        device.put("firmware_version", "");
                        device.put("build_date", "");
                        device.put("hardware_id", "");
                        device.put("stream_urls", new JSONArray());
                        device.put("camera_status", true);
                        device.put("connected", false);
                        array.add(device);
                    }
                }
            }
        }
        messagingTemplate.convertAndSend("/topic/not-approved-cameras", array.toString());
        List<String> inactiveCameraIdList = determineInactiveCameras(discoveredUuidList, existingUuidList);
        array = new JSONArray();
        if (inactiveCameraIdList.size() > 0) {
            for (String uuid : inactiveCameraIdList) {
                DeviceInfoPlainDto deviceInDB = deviceInfoService.getDeviceInfoByUuid(uuid);
                JSONObject device = new JSONObject();
                device.put("uuid", uuid);
                device.put("ip", deviceInDB.getIpAddressList()[0]);
                device.put("name", deviceInDB.getModel());
                device.put("hardware", "");
                device.put("serial_number", deviceInDB.getSerialNumber());
                device.put("firmware_version", deviceInDB.getFirmwareVersion());
                device.put("build_date", deviceInDB.getBuildDate());
                device.put("hardware_id", deviceInDB.getHardwareId());
                JSONArray urlArray = new JSONArray();
                String[] streamUrls = deviceInDB.getStreamUrlList();
                urlArray.addAll(Arrays.asList(streamUrls));
                device.put("stream_urls", urlArray);
                device.put("camera_status", false);
                device.put("connected", false);
                //TODO: use sql batch process!!!
                deviceInfoService.updateDeviceActive(uuid, false);
                array.add(device);
            }
        }
        messagingTemplate.convertAndSend("/topic/inactive-cameras", array.toString());
        return "OK";
    }

    private List<String> determineApprovedCameras(List<String> discoveryUuidList, List<String> existingUuidList) {
        return discoveryUuidList.stream().filter(uuid -> existingUuidList.contains(uuid)).collect(Collectors.toList());
    }

    private List<String> determineNotApprovedCameras(List<String> discoveryUuidList, List<String> existingUuidList) {
        return discoveryUuidList.stream().filter(uuid -> !existingUuidList.contains(uuid)).collect(Collectors.toList());
    }

    private List<String> determineInactiveCameras(List<String> discoveryUuidList, List<String> existingUuidList) {
        return existingUuidList.stream().filter(uuid -> !discoveryUuidList.contains(uuid)).collect(Collectors.toList());
    }
}
